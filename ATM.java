

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
public class ATM extends JFrame implements ActionListener
{    
   
   JButton b[]=new JButton[15];
    
   int i,r,n1,n2;
   JTextField res;
   JTextField input;
   JTextArea area;
   char op; 
   int count =0;
   Bank bank = new Bank();
   String text ="";          
   
   public ATM() {
	   JFrame  frame = new JFrame("ATM");
	   frame.setSize(410, 450);
	   frame.setDefaultCloseOperation(1);
	   frame.setLayout(null);
	   
	   
	   input = new JTextField();
	   input.setBounds(100, 10, 210, 40);
	   frame.add(input);
	   
	   for(int i=0;i<10;i++) {
		   b[i]=new JButton();
		   b[i].setText(i+"");
		   b[i].addActionListener(this);
	   }
	   b[7].setBounds(100, 50, 70, 40);
	   frame.add(b[7]);
	   
	   b[8].setBounds(170, 50, 70, 40);
	   frame.add(b[8]);
	   
	   b[9].setBounds(240, 50, 70, 40);
	   frame.add(b[9]);
	   
	   b[4].setBounds(100, 90, 70, 40);
	   frame.add(b[4]);
	   
	   b[5].setBounds(170, 90, 70, 40);
	   frame.add(b[5]);
	   
	   b[6].setBounds(240, 90, 70, 40);
	   frame.add(b[6]);
	   
	   b[1].setBounds(100, 130, 70, 40);
	   frame.add(b[1]);
	   
	   b[2].setBounds(170, 130, 70, 40);
	   frame.add(b[2]);
	   
	   b[3].setBounds(240, 130, 70, 40);
	   frame.add(b[3]);
	   
	   b[0]=new JButton();
	   b[0].setText("0");
	   b[0].addActionListener(this);
	   b[0].setBounds(100, 170, 70, 40);
	   frame.add(b[0]);
	   
	   b[10]=new JButton();
	   b[10].setText(".");
	   b[10].addActionListener(this);
	   b[10].setBounds(170, 170, 70, 40);
	   frame.add(b[10]);
	   
	   b[11]=new JButton();
	   b[11].setText("CE");
	   b[11].addActionListener(this);
	   b[11].setBounds(240, 170, 70, 40);
	   frame.add(b[11]);
	   
	  area = new JTextArea();
	   area.setText("Enter customer number\nA=OK");
	   area.setBounds(100, 250, 210, 70);
	   frame.add(area);
	   
	   b[12]=new JButton();
	   b[12].setText("A");
	   b[12].addActionListener(this);
	   b[12].setBounds(100, 350 , 70, 40);
	   frame.add(b[12]);
	   
	   b[13]=new JButton();
	   b[13].setText("B");
	   b[13].addActionListener(this);
	   b[13].setBounds(170, 350, 70, 40);
	   frame.add(b[13]);
	   
	   b[14]=new JButton();
	   b[14].setText("C");
	   b[14].addActionListener(this);
	   b[14].setBounds(240, 350, 70, 40);
	   frame.add(b[14]);
	   
	   frame.setResizable(false);
	   frame.setVisible(true);
	   
   }
    
 
  public static void main(String arg[])
  {
      new ATM();
  }


	@Override
	public void actionPerformed(ActionEvent e) {
		String a = e.getActionCommand();
		if(a!="A"&&a!="B"&&a!="C"&&a!="CE") {
			text = input.getText()+a;
			input.setText(text);
		}
		
		
		if(a=="A" &&count==0&&bank.checkID(input.getText())) {
			count++;
			input.setText("");
			text="";
			area.setText("Enter PIN\r\n" + 
					"A = OK");
			
			
		}
		if(a=="A" &&count==1&&bank.checkPin(input.getText())) {
			input.setText("");
			text="";
			area.setText("Enter PIN\r\n" + 
					"A = OK");
			count++;
		}
		if(count == 2) {
			area.setText("Select Account\n A = Checking\n B = Savings\n C = Exit");
			count++;
		}
		if(count == 3&&(a=="A"||a=="B")) {
			area.setText("A = Withdraw\r\n" + 
					"B = Deposit\r\n" + 
					"C = Cancel");
			count++;
		}
		if(count == 4&&(a=="A"||a=="B")) {
			a="";
			area.setText("Balance="+bank.amount+"\nEnter amount\nA=OK\n");
			count++;
		}
		if(count == 5&&(a=="A")) {
			bank.amount=bank.amount-Double.parseDouble(input.getText());
			a="C";
			count=3;
		}
		if(count == 5&&(a=="B")) {
			bank.amount=bank.amount+Double.parseDouble(input.getText());
			count=3;
		}
		if(count == 4&&(a=="C")) {
			
			count=3;
		}
		if(count == 3&&a=="C") {
			input.setText("");
			area.setText("Enter customer number\nA=OK");
			count=0;
		}
		
		
		// TODO Auto-generated method stub
		
	}
}