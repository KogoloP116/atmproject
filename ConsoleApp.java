	
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleApp {
	
	
	  public static void main(String[] args) throws IOException {
		  
		  BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		  Bank bank = new Bank();
		  
		  while(true) {
		  System.out.println("Enter customer number:");
		  String id = br.readLine();
		  if(bank.checkID(id)) {
			  System.out.println("Enter Pin:");
			  String pin = br.readLine();
			  if(bank.checkPin(pin)) {
				  System.out.println("A=Checking, B=Savings, C=Quit:");
				  String opt = br.readLine();
				  if(opt.equals("A")||opt.equals("B")) {
					  System.out.println("Amount:"+bank.amount);
					  System.out.println("A=Deposit, B=Withdrawal, C=Cancel: ");
					  String op = br.readLine();
					  if(op.equals("A")) {
						  System.out.println("Amount:");
						  String amt = br.readLine();
						  double amt1 = Double.parseDouble(amt);
						  bank.amount=bank.amount+amt1;
					  }
					  if(op.equals("B")) {
						  System.out.println("Amount:");
						  String amt11 = br.readLine();
						  double amt12 = Double.parseDouble(amt11);
						  bank.amount=bank.amount-amt12;
					  }
				  }
			  }
		  }
			  
		  }
	  }

}
